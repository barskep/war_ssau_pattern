﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainStand : MonoBehaviour {

    #region Singleton
    public static MainStand Instance
    {
        get
        {
            return Instance;
        }
        private set
        {
            if (Instance == null)
                Instance = value;
        }
    }
    #endregion

    
}
