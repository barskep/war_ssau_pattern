﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITabsPanel : MonoBehaviour
{

    [SerializeField] private GameObject tabItemPrefab;
    [SerializeField] private List<TabSettings> tabSettings;
    [SerializeField] private Transform root;
    [SerializeField] private GameObject acceptableExit;
    
    void Start()
    {
        for (int i = 0; i < tabSettings.Count; ++i)
        {
            var tab = Instantiate(tabItemPrefab, root);
            var uiTab = tab.GetComponent<UITabItem>();
            uiTab.Init(tabSettings[i]);
            if (i == 0) uiTab.OnClickTab();
        }
    }

    public void ClickExit()
    {
        acceptableExit.gameObject.SetActive(!acceptableExit.activeSelf);
    }

    public void ClickAcceptExit()
    {
        Application.Quit();
    }

}

[System.Serializable]
public class TabSettings
{
    public Dashboard dashboard;
    public Sprite sprite;
    public string name;
}
