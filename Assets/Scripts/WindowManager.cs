﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManager : MonoBehaviour
{

	private IWindow _currentWindow;
	
	private void Awake()
	{
		Instance = this;
	}

	public void OpenWindow(IWindow window)
	{
		if (_currentWindow != null)
			_currentWindow.OnClose();

		if (window is MonoBehaviour)
		{
			var w = (MonoBehaviour) window;
			w.gameObject.SetActive(true);
		}
		
		window.OnShow();
		_currentWindow = window;
	}

	#region Singleton
	public static WindowManager Instance { get; private set; }
	#endregion
}
