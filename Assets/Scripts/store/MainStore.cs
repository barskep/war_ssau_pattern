﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public class MainStore : MonoBehaviour
{
    /*
    * With the current architecture, the responsibility
    * in initializing the state lies with the controller
    * that uses this state.
    */
    public AudioSource _player;

    private ReplaySubject<Unit> _replayUpdate;

    public IObservable<Unit> OnUpdate
    {
        get { return _replayUpdate; }
    }


    private void Awake()
    {
        _replayUpdate = new ReplaySubject<Unit>();
        Instance = this;
    }

    public static MainStore Instance { get; private set; }

    private static MainStore _instance;

    private MainStore()
    {
    }

    public void DispatchAction(object action)
    {
        //Dispatch code
        _replayUpdate.OnNext(Unit.Default);
    }
    
}
