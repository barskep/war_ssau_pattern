﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dashboard : MonoBehaviour, IWindow
{

    public virtual void OnShow()
    {
        
    }

    public virtual void OnClose()
    {
        gameObject.SetActive(false);
    }
}

public interface IWindow
{
    void OnShow();
    void OnClose();
}
