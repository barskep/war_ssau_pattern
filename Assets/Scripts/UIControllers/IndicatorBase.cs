﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IndicatorBase : ControllerBase {
    public abstract float ResetValue();

    public abstract void SetOverTopValue();
}
