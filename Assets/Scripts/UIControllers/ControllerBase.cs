﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Serialization;

public abstract class ControllerBase : MonoBehaviour {

    [SerializeField] protected bool isReverse = false;
    protected bool isClockwise = true;
    
    public virtual int NumberOfStates
    {
        get { return settings.numberOfStates; }
    }

    public virtual float MinValue
    {
        get { return settings.minValue; }
    }

    public virtual float MaxValue
    {
        get { return settings.maxValue; }
    }

    public virtual float DefaultValue
    {
        get { return settings.defaultValue; }
    }

    public virtual float ArcLength
    {
        get { return MaxValue - MinValue; }
    }

    public virtual float StateAngle
    {
        get
        {
            return ArcLength / (NumberOfStates - 1);
        }
    }

    public virtual float DeltaAngle
    {
        get
        {
            if (isReverse)
            {
                if (StateIndex + 1 >= NumberOfStates)
                    isClockwise = false;
                else if (StateIndex <= 0)
                    isClockwise = true;

                if (!isClockwise)
                    return -StateAngle;
                
            }
                
            return StateAngle;
        }
    }

    public virtual int StateIndex
    {
        get
        {
            if (NumberOfStates == 0) return 0;
            return Mathf.RoundToInt((CurrentValue - MinValue) / StateAngle);
        }
    }
    //текущее значение угла
    [SerializeField, HideInInspector] protected float currentValue;
    public abstract float CurrentValue { get; set; }

    [SerializeField] protected ValueSettings settings;

    [FormerlySerializedAs("OnValueChanged")] public Action<float> onValueChanged;

    private void Start()
    {
        currentValue = DefaultValue;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, -currentValue));
        if (onValueChanged != null)
            onValueChanged(DefaultValue);
    }
}

[Serializable]
public class ValueSettings
{
    //Минимальное значение
    public float minValue;
    //максимальное значения
    public float maxValue;
    //значение по умолчанию
    public float defaultValue;
    //количество состояний
    public int numberOfStates;
    //вбез ограничений по вращению
    public bool withoutLimit;
}
