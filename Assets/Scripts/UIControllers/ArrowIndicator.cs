﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowIndicator : MonoBehaviour
{

	[SerializeField] private float minAngle;
	[SerializeField] private float maxAngle;
	[SerializeField] private float defaultValue;
	[SerializeField] private float animationSpeed = 0.05f;
	[SerializeField] private bool isClamp01 = true;
	private float startValue, endValue;
	private float currentValue;
	private Coroutine animationCoroutine;

	private void OnEnable()
	{
		CurrentValue = currentValue;
	}

	public float Speed01ToAngles
	{
		get { return animationSpeed; }
		set { animationSpeed = value * (maxAngle - minAngle); }
	}

	[SerializeField] private Transform arrowTransform;
	public float CurrentValue
	{
		get { return currentValue; }
		set
		{
			var temp = isClamp01 ? Mathf.Clamp01(value) : value;
			var lastValue = currentValue;
			currentValue = lastValue > 1 && temp > 1 ? temp % 1 : temp;
			var arcLength = maxAngle - minAngle;
			var angle = currentValue * arcLength + minAngle;
			if (animationCoroutine != null)
				StopCoroutine(animationCoroutine);
			
			if (isActiveAndEnabled)
			{
				endValue = angle;
			}
		}
	}

	public void SetValueImmediately(float value)
	{
		CurrentValue = value;
		startValue = value * (maxAngle - minAngle) + minAngle;
		arrowTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, -startValue));
	}


	void Start ()
	{
		SetValueImmediately(defaultValue);
		
	}

	private void FixedUpdate()
	{
		bool clockWise = startValue < endValue;
		startValue += clockWise ? animationSpeed : -animationSpeed;
		arrowTransform.rotation = Quaternion.Euler(new Vector3(0, 0, -startValue));
		if (clockWise && startValue > endValue || !clockWise && endValue > startValue)
		{
			arrowTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, -endValue));
			startValue = endValue;
		}
	}

	/*private IEnumerator AnimateRotation(float startValue, float endValue)
	{
		bool clockWise = startValue < endValue;
		while (true)
		{
			startValue += clockWise ? animationSpeed : -animationSpeed;
			arrowTransform.rotation = Quaternion.Euler(new Vector3(0, 0, -startValue));
			if (clockWise && startValue > endValue || !clockWise && endValue > startValue)
			{
				arrowTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, -endValue));
				yield break;
			}

			yield return null;

		}
	}*/

	/*private IEnumerator ValueChanger()
	{
		bool clockWise = true;
		while (true)
		{
			var newval = Random.Range(0.00f, 1.00f);
			CurrentValue = newval;
			yield return new WaitForSeconds(1f);
		}
	}*/
}
