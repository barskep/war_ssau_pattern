﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

[Serializable]
public class ObservableController<T> : MonoBehaviour
{
	public IObservable<T> Observable
	{
		get { return Replay; }
	}
	protected ReplaySubject<T> Replay = new ReplaySubject<T>();
}
