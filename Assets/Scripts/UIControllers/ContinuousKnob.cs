﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ContinuousKnob : ControllerBase, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public override float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (Math.Abs(value - currentValue) > Mathf.Epsilon)
            {
                currentValue = value;
                
                if (!settings.withoutLimit)
                    currentValue = Mathf.Clamp(currentValue, settings.minValue, settings.maxValue);
                
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, -currentValue));

                if (onValueChanged != null)
                    onValueChanged(currentValue);
            }
        }
    }

    private Vector2 _lastPos;
    private Camera _mainCamera;

    private void Start()
    {
        _mainCamera = Camera.main;
        CurrentValue = settings.defaultValue;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _lastPos = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Получаем текущую позицию в экранных координатах
        Vector2 currentPosition = _mainCamera.WorldToScreenPoint(transform.position);
        Vector2 delta = eventData.position - _lastPos;
        Vector2 maxParam = GetMaxParam(delta);
        Vector2 relativePos = eventData.position - currentPosition;
        //Находим стороны треугольника
        float aTriangle = (_lastPos - currentPosition).magnitude;
        float bTriangle = (eventData.position - currentPosition).magnitude;
        float cTriangle = delta.magnitude;

        //Вычисляем угол по трем сторонам
        float angle = GetAngleOfTriangle(aTriangle, bTriangle, cTriangle);

        if (relativePos.x > 0)
        {
            if (relativePos.y > 0)
            {
                if (maxParam.x > 0 || maxParam.y < 0)
                    CurrentValue += angle;
                else CurrentValue -= angle;
            }
            else
            {
                if (maxParam.x < 0 || maxParam.y < 0)
                    CurrentValue += angle;
                else CurrentValue -= angle;
            }
        }
        else
        {
            if (relativePos.y > 0)
            {
                if (maxParam.x > 0 || maxParam.y > 0)
                    CurrentValue += angle;
                else CurrentValue -= angle;
            }
            else
            {
                if (maxParam.x < 0 || maxParam.y > 0)
                    CurrentValue += angle;
                else CurrentValue -= angle;
            }
        }

        _lastPos = eventData.position;  
    }

    static Vector2 GetMaxParam(Vector2 delta)
    {
        if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
        {
            return new Vector2(delta.x, 0);
        }
        return new Vector2(0, delta.y);
    }

    /// <summary>
    /// Возвращает угол, между сторонами a и b по длинам трех сторон
    /// </summary>
    /// <returns></returns>
    static float GetAngleOfTriangle(float aTriangle, float bTriangle, float cTriangle)
    {
        return Mathf.Acos((aTriangle * aTriangle + bTriangle * bTriangle - cTriangle * cTriangle)
                          / (2 * aTriangle * bTriangle)) * Mathf.Rad2Deg;
    }

    public void OnEndDrag(PointerEventData eventData)
    {

    }
}
