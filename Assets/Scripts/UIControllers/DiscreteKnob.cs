﻿using System.Collections;
using System;
using UnityEngine;

public class DiscreteKnob : ControllerBase
{
    [Range(0.01f, 1f)]
    [SerializeField] private float animSpeed = 0.05f;

    public override float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            var defaultValue = -currentValue;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, -currentValue));
            if (Mathf.Abs(value) > settings.maxValue)
            {
                currentValue = settings.minValue;
                if (onValueChanged != null)
                    onValueChanged(currentValue);
            }
            else if (Math.Abs(value - currentValue) > Mathf.Epsilon)
            {
                currentValue = value;
                if (onValueChanged != null)
                    onValueChanged(currentValue);
            }
            else
            {
                Debug.LogError("Error");
            }
            StopAllCoroutines();

            if (defaultValue < -currentValue)
                defaultValue += 360;
            
            StartCoroutine(SmoothChange(defaultValue, -currentValue));
        }
    }

    public void OnClickKnob()
    {
        CurrentValue += DeltaAngle;
    }

    private IEnumerator SmoothChange(float defaultAngle, float angle)
    {
        float progress = 0;

        if (!isClockwise)
            defaultAngle -= 360;
        
        while (progress <= 1)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Lerp(defaultAngle, angle, progress)));
            progress += animSpeed;
            yield return null;
        }
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        yield return null;
    }
}
