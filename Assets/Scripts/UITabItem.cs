﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITabItem : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Text text;
    private TabSettings _settings;

    public void Init(TabSettings settings)
    {
        _settings = settings;
        image.sprite = settings.sprite;
        text.text = settings.name;
    }

    public void OnClickTab()
    {
        WindowManager.Instance.OpenWindow(_settings.dashboard);
    }

}
